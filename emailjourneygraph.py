import matplotlib.pyplot as plt
import networkx as nx

# Create the graph object and add nodes and edges
G = nx.DiGraph()
nodes = ["Sender's Device", "Sender's Email Server", "Recipient's Email Server",
         "Recipient's Laptop", "Recipient's Smartphone", "Backups", "Archiving Services",
         "Sender's Device Backup", "Recipient's Laptop Backup", "Recipient's Smartphone Backup"]
edges = [("Sender's Device", "Sender's Email Server"),
         ("Sender's Email Server", "Recipient's Email Server"),
         ("Recipient's Email Server", "Recipient's Laptop"),
         ("Recipient's Email Server", "Recipient's Smartphone"),
         ("Sender's Email Server", "Backups"),
         ("Recipient's Email Server", "Backups"),
         ("Recipient's Email Server", "Archiving Services"),
         ("Sender's Device", "Sender's Device Backup"),
         ("Recipient's Laptop", "Recipient's Laptop Backup"),
         ("Recipient's Smartphone", "Recipient's Smartphone Backup")]

G.add_nodes_from(nodes)
G.add_edges_from(edges)

# Node positions
pos = nx.circular_layout(G)

# Differentiating node shapes and colors
shapes = {"Devices": "s", "Services": "o"}
color_scheme = {"Devices": "lightgreen", "Services": "lightskyblue"}
categories = {
    "Devices": ["Sender's Device", "Recipient's Laptop", "Recipient's Smartphone"],
    "Services": ["Sender's Email Server", "Recipient's Email Server", "Backups",
                 "Archiving Services", "Sender's Device Backup", "Recipient's Laptop Backup",
                 "Recipient's Smartphone Backup"]
}

# Correct calculation for the potential total number of copies
num_servers = 2  # sender's, recipient's (CC/BCC Recipients are not visualized but included in text)
num_devices = 5  # sender's device, recipient's laptop, recipient's smartphone, and backups for each
num_backups = num_servers + num_devices  # assuming each server and device has a backup
num_archiving_services = 1  # assuming one archiving service
num_sent_folders = 1  # typically only the sender's device

# Summing up all the copies
total_copies = num_servers + num_devices + num_backups + num_archiving_services + num_sent_folders

# Legend patches for the shapes
devices_patch = plt.Line2D([0], [0], marker='s', color='w', label='Devices',
                           markersize=10, markerfacecolor='lightgreen')
services_patch = plt.Line2D([0], [0], marker='o', color='w', label='Services',
                            markersize=10, markerfacecolor='lightskyblue')

# Generating the graph with adjusted layout
fig, ax = plt.subplots(figsize=(14, 10))
for category, nodes in categories.items():
    nx.draw_networkx_nodes(G, pos, nodelist=nodes, node_shape=shapes[category],
                           node_color=color_scheme[category], node_size=3500, ax=ax)
nx.draw_networkx_edges(G, pos, edge_color='gray', arrows=True, node_size=3500, arrowstyle='->', arrowsize=15, ax=ax)
nx.draw_networkx_labels(G, pos, font_color='black', font_size=12, ax=ax)

# Updated legend text
calculation_text = (f"Potential Total Copies: {total_copies}\n"
                    "Includes copies on servers, devices, backups,\n"
                    "archiving services, and 'Sent' folders.\n"
                    "This number multiplies with each CC/BCC recipient.")

# Adjusting the legends
ax.legend(handles=[devices_patch, services_patch], loc='lower left', 
          bbox_to_anchor=(0, -0.15), ncol=1, borderaxespad=0, frameon=False)
ax.text(0.5, -0.15, calculation_text, wrap=True, horizontalalignment='center', fontsize=12, 
        bbox=dict(facecolor='white', edgecolor='none', boxstyle='round,pad=0.5'), 
        transform=ax.transAxes)

plt.title("Journey of an Email with Attachments")
plt.subplots_adjust(bottom=0.25)  # Adjust the bottom to fit legends

# Save the graph as a PNG file
plt.savefig('email_journey_graph.png', bbox_inches='tight', dpi=300)

# Save the graph as a PDF file
plt.savefig('email_journey_graph.pdf', bbox_inches='tight', dpi=300)

plt.show()
